<?php
  define('DB_ADAPTER', 'mysql'); 
  define('DB_HOST', 'localhost'); 
  define('DB_USER', 'root'); 
  define('DB_PASS', 'root'); 
  define('DB_NAME', 'feng_groupware'); 
  define('DB_PERSIST', true); 
  define('TABLE_PREFIX', 'fo_'); 
  define('DB_ENGINE', 'InnoDB'); 
  define('ROOT_URL', 'http://localhost'); 
  define('DEFAULT_LOCALIZATION', 'en_us'); 
  define('COOKIE_PATH', '/'); 
  define('DEBUG', false); 
  define('SEED', 'db823a4449db53d18f4b9b59d54322b3'); 
  define('DB_CHARSET', 'utf8'); 
  return true;
?>